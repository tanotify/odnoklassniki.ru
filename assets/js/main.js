/*
    Торопанов Антон - Тестовое задание
 */

$(window).ready(function(){
    var visibleItems = 3;

    var app = {

        init: function () {
            app.loadData("mine");
            app.loadData("friend");

            app.buttonsActions();
        },

        // Шаблон загрузки элемента списка
        loadTemplate: function (div, id, text, hidden) {
            var hiddenClass = "";
                filter = app.dataFilter(div.data("list")),
                countLi = Object.keys(filter[0].items).length;

            if (id > visibleItems - 1) { hiddenClass = "hidden "; }
            if (!div.hasClass("has-hidden")){ hiddenClass = ""; }

            div.append('\
                <li data-item="' + id + '" class="' + hiddenClass + 'h-list_li">\
                    <span class="h-list_li_name">' + text + '</span>\
                    <span class="h-icon_was-added show-if-success">добавлено в ваши увлечения</span>\
                    ' + (div.attr('id') == 'h-list-data_friend' ? '<span class="h-icon_add show-on-hover"></span>': '') + '\
                    ' + (div.attr('id') == 'h-list-data_mine' ? '<span class="h-icon_remove show-on-hover"></span>': '') + '\
                    <span class="h-icon_report show-on-hover">\
                    <span class="h-icon_report_label">пожаловаться</span>\
                    <span class="h-icon_report_icon"></span>\
                    <span class="h-icon_report_form">\
                    <span>Причина удаления</span>\
                    <textarea name="" id="report_text" cols="30" rows="10"></textarea>\
                    <button>Отправить</button>\
                    </span>\
                    </span>\
                </li>\
            ');
        },

        // Предзагрузка данных из JSON файла
        loadData: function (listId) {
            $.ajax({
                url: "data.json",
                dataType: "json"
            }).done(function(data) {

                app.hobbies = data;
                var filter = app.dataFilter(listId),
                    container = $('#h-list-data_' + listId),
                    countLi = $("#h-list-data_" + listId + " > li ").length;

                container.addClass("has-hidden");

                $.each(filter[0].items, function(i, elem) {
                    app.loadTemplate(container, elem.id, elem.text, 1);
                });

                if (countLi - 1 > visibleItems) {
                    app.checkCount(listId, countLi-1);
                }

            }).fail(function(){
                console.log("Что-то поломалось!");
            });
        },

        // Фильтрация данных из базы данных
        dataFilter:function (list) {
            var filter = app.hobbies.filter(function(item) { return item.list === list; });
            return filter;
        },

        // Сброс id элементов
        idReset:function(container, itemId) {
            var i = 1;
            if (itemId == 0) { i = 0; }
            container.find("li").each( function() {
                if($(this).data("item")) {
                    $(this).attr('data-item', i);
                    i++;
                }
            });
        },

        // Подсчет элементов списка и их частичное скрытие
        checkCount: function (list, count) {
            var container = $('#h-list-data_' + list),
                count = count - visibleItems;

            container.find('.h-list_more').remove();

            if (container.hasClass("has-hidden") && count > 0){
                container.append('\
                <li class="h-list_li h-list_more">\
                    <a href="#">еще ' + count + ' интереса</a>\
                </li>');
            }

        },

        // События на нажатия кнопок
        buttonsActions: function () {
            $(".h-icon_report_label").live('click', this.reportHobby);
            $(".h-icon_remove").live('click', this.removeHobby);
            $(".h-icon_add").live('click', this.moveHobby);
            $(".h-list_more").live('click', this.showHidden);

            $(".h-list_input-add input").keyup(function(event){
                if(event.keyCode == 13){
                    var hobbiesList = $(this).data("list");
                    var value = $(this).val();
                    app.addHobby(hobbiesList, value);
                    $(this).val("");
                }
            });
        },

        // Добавление новой записи
        addHobby: function(hobbiesList, value){
            var filter = app.dataFilter(hobbiesList),
                countList = Object.keys(filter[0].items).length,
                container = $('#h-list-data_' + hobbiesList);

            var tempItem = {
                "id" : countList,
                "text" : value,
                "reported" : "false"
            };

            filter[0].items.push(tempItem);

            app.loadTemplate(container, countList, value, false);
            app.checkCount(hobbiesList, Object.keys(filter[0].items).length);
            app.idReset(container, 1);
        },

        // Удаление записи
        removeHobby: function(list, itemId){
            var list = $(this).closest("ul").data("list"),
                itemId = $(this).closest("li").data("item");

            $(this).parents("ul").find(".hidden:first").removeClass("hidden");
            $(this).parent("li").remove();

            var filter = app.dataFilter(list),
                container = $('#h-list-data_' + list);

            filter[0].items.pop(itemId);

            app.checkCount(list, Object.keys( filter[0].items).length);
            app.idReset(container, itemId);

        },

        // Пермещение записи
        moveHobby: function(){
            var list = $(this).closest("ul").data("list"),
                itemId = $(this).closest("li").data("item"),
                tempItemText = $(this).parent().find(".h-list_li_name").text();

            var currentList = app.dataFilter("friend"),
                container = $('#h-list-data_' + list);

            $(this).parents("ul").find(".hidden:first").removeClass("hidden");
            $(this).parent("li").remove();

            currentList[0].items.pop(itemId);
            app.addHobby("mine", tempItemText);
            app.idReset(container, 0);

            app.checkCount(list, Object.keys(currentList[0].items).length);
        },

        // Форма репорта
        reportHobby: function(){
            var container = $(this).parent(),
                form = container.find(".h-icon_report_form"),
                textarea = $("#report_text").val();

            if(!container.hasClass("poped")) {
                container.addClass("poped");
            } else {
                container.removeClass("poped");
            }

            form.find("button").click(function() {
                    alert("Спасибо за ваше сообщение! В течение этого дня " +
                    "администрация сайта рассмотрит заявку и примет решение по данному пункту.");
                    container.removeClass("poped");
            });

        },

        // Показ всех элементов
        showHidden: function(){
            var list = $(this).closest("ul").attr('id');

            $("#" + list + " > li").each(function() {
                $(this).removeClass("hidden");
            });
            $("#" +list).find(".h-list_more").remove();
            $("#" +list).removeClass("has-hidden");
            return true;
        }
    }


    app.init();
});
