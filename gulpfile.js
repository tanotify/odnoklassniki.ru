var gulp = require('gulp');

var minifyCSS = require('gulp-minify-css'),
    minifyJS = require('gulp-uglify'),
    rename = require("gulp-rename");

gulp.task('css', function() {
    gulp.src('assets/css/*.css')
        .pipe(minifyCSS({keepBreaks:false}))
        .pipe(rename({ suffix: ".min" }))
        .pipe(gulp.dest('assets/compiled'))
});

gulp.task('js', function() {
    gulp.src('assets/js/*.js')
        .pipe(minifyJS())
        .pipe(rename({ suffix: ".min" }))
        .pipe(gulp.dest('assets/compiled'))
});

gulp.task('default', function() {
    gulp.run('css', 'js');
});